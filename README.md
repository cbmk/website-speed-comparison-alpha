# website-speed-comparison

## Project setup
```
npm install
```

### Starts the runner
```
npm run exec -- --file=storage/test.csv
```

wait the end of the job

### Change number of tries
```
npm run exec -- --file=storage/test.csv --tries=33
```

### Restart from a specific chunk
```
npm run exec -- --file=storage/test.csv --chunk=1
```

### Example CSV input

```
host,path,type
http://demo-it.getwebspark.com/,/,HOME
http://demo-it.getwebspark.com/,/?__classic=1,HOME_CLASSIC
http://demo-it.getwebspark.com/,/auto/usate/{VDP},VDP
```

* `host` column is the host of the websites
* `path` column is the path of the websites
* `type` column is an type of the page, is just an identifier 