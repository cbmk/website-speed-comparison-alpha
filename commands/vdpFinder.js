const axios = require('axios');
const cheerio = require('cheerio');

const vdpUrlCache = {}

module.exports = async (listingUrl) => {

  if (vdpUrlCache.hasOwnProperty(listingUrl)) {
    return vdpUrlCache[listingUrl]
  }

  try {
    const html = await axios.get(listingUrl).then(response => response.data)
    const $ = cheerio.load(html);

    let vdpUrl = ''

    /**
     * Stock Listing V3 matcher
     */
    const v3Regex = /<a href="([^"]*)" class="c-vehicle-card--row__media-grid /;
    let v3MatchResult = v3Regex.exec(html)

    if (v3MatchResult) {
      vdpUrl = v3MatchResult[1]
    }
    /**
     * Classic cards
     */
    else if ($('.c-vehicle-card__main').length > 0 ) {
      vdpUrl = $('.c-vehicle-card__main').first().attr('href')
    } else if ($('.c-vehicle-card--row__media-grid-holder').length > 0 ) {
      vdpUrl = $('.c-vehicle-card--row__media-grid-holder').first().attr('href')
    } else if ($('.car-card-stock__link-block').length > 0) {
      vdpUrl = $('.car-card-stock__link-block').first().attr('href')
    }
    /**
     * (R)Evolution cards
     */
    else if ($('.vcard--link').length > 0 ) {
      vdpUrl = $('.vcard--link').first().attr('href')
    }

    vdpUrlCache[listingUrl] = vdpUrl
    return (vdpUrl.length > 0 ) ? vdpUrl : null

  } catch {
  }
  return null
}