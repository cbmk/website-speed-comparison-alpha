require("console-stamp")(console, "[HH:MM:ss.l]");
const fs = require("fs");
const psi = require("psi");
const axios = require("axios");
const _ = require("lodash");
const path = require("path");
const parse = require("csv-parse");
const yargs = require("yargs/yargs");
const differenceInSeconds = require("date-fns/differenceInSeconds");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const argv = yargs(process.argv).parse();

const vdpFinder = require("./vdpFinder");
const GOOGLE_API_KEYS = require("./api");
const STATISTICS_STRATEGIES = ["desktop", "mobile"];

const UserAgents = {
  mobile:
    "Mozilla/5.0 (Linux; Android 7.0; Moto G (4)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4143.7 Mobile Safari/537.36 Chrome-Lighthouse",
  desktop:
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Chrome-Lighthouse"
};

const sleep = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

const createExportFile = path =>
  createCsvWriter({
    append: true,
    path,
    header: [
      { id: "chunkId", title: "chunkId" },
      { id: "type", title: "type" },
      { id: "strategy", title: "strategy" },
      { id: "host", title: "host" },
      { id: "path", title: "path" },
      { id: "performance_result", title: "Score" },
      { id: "cumulative_layout_shift", title: "CLS" },
      { id: "first_contentful_paint", title: "FCP" },
      { id: "largest_contentful_paint", title: "LCP" },
      { id: "time_to_interactive", title: "TTI" },
      { id: "total_blocking_time", title: "TBT" },
      { id: "initial_server_response_time", title: "ISRT" },
      { id: "javascript_execution_time", title: "JSExecTime" },
      { id: "defer_offscreen_images", title: "DeferOffscreenImages" },
      { id: "excessive_dom_size", title: "ExcessiveDomSize" },
      { id: "impact_third_party_code", title: "ImpactThirdPartyCode" },
      { id: "main_thread_work", title: "MainThreadWork" },
      { id: "not_efficient_assets_cache", title: "NotEfficientAssetsCache" },
      { id: "render_blocking_resources", title: "RenderBlockingResources" },
      { id: "speed_index", title: "SpeedIndex" }
      //{ id: "unused_css", title: "UnusedCss" },
      //{ id: "unused_css_potential_saving", title: "UnusedCssPotentialSaving" },
      //{ id: "unused_js", title: "UnusedJs" },
      //{ id: "unused_js_potential_saving", title: "UnusedJsPotentialSaving" }
    ]
  });

const call = (
  result,
  { apiKey, chunkId, exportFileWriter, exportErrorFileWriter }
) => {
  return new Promise(async resolve => {
    let cached = false;
    let counter = -1;
    let skip = false;

    let host = result.host.endsWith("/")
      ? result.host.slice(0, -1)
      : result.host;
    let path = result.path;
    let qs = "";
    if (path.includes("?")) {
      const [p, q] = path.split("?");
      qs = q;
      path = p;
    }

    const tsBeforeCall = new Date().getTime();
    const queryString = '';//qs.length
      //? `?${qs}&__ts=${tsBeforeCall}`
      //: `?__ts=${tsBeforeCall}`;
    let fakeUrl = `${host}${path}${queryString}`;

    if (path.includes("{VDP}")) {
      const vdpUrl = await vdpFinder(`${host}${path.replace("{VDP}", "")}`);
      if (!vdpUrl) {
        exportErrorFileWriter.writeRecords({
          host,
          path,
          fakeUrl,
          error: "IMPOSSIBLE_TO_FIND_VDP"
        });
        console.log(`IMPOSSIBLE TO FIND VDP: ${host} - ${path} - ${fakeUrl}`);
        return resolve(false);
      }
      fakeUrl = `${vdpUrl}${queryString}`;
    }

    //do {
    counter++;

    try {
      const cacheValue = await axios.get(fakeUrl, {
        headers: {
          "User-Agent": UserAgents[result.strategy]
        }
      });
      const data = cacheValue.data;

      cached =
        data.includes("served from batcache") ||
        data.includes("bytes batcached for");
      if (cached) {
        //break;
      }
    } catch (error) {
      console.log("Error", `${fakeUrl} - ${error} (${counter})`);
      skip = true;
      //break;
    }
    //} while (cached !== true || skip !== true);

    if (skip) {
      console.log(`ROTTO: ${counter} - ${fakeUrl}`);
      exportErrorFileWriter.writeRecords({
        host,
        path,
        fakeUrl,
        error: "BROKEN_SITE"
      });
      return resolve(false);
    }

    console.log(`BEFORECALL: ${counter} - ${fakeUrl}`);
    try {
      const response = await psi(fakeUrl, {
        key: apiKey,
        strategy: result.strategy
      });
      console.log(`CALLDONE: ${fakeUrl}`);

      const psiResult = response.data;

      const dataResult = {
        chunkId: chunkId,
        host: result.host,
        path: result.path,
        type: result.type,
        strategy: result.strategy,
        performance_result:
          _.get(psiResult, "lighthouseResult.categories.performance.score") *
          100,
        first_contentful_paint: _.get(
          psiResult,
          "lighthouseResult.audits.first-contentful-paint.numericValue"
        ),
        speed_index: _.get(
          psiResult,
          "lighthouseResult.audits.speed-index.numericValue"
        ),
        largest_contentful_paint: _.get(
          psiResult,
          "lighthouseResult.audits.largest-contentful-paint.numericValue"
        ),
        time_to_interactive: _.get(
          psiResult,
          "lighthouseResult.audits.interactive.numericValue"
        ),
        total_blocking_time: _.get(
          psiResult,
          "lighthouseResult.audits.total-blocking-time.numericValue"
        ),
        cumulative_layout_shift: _.get(
          psiResult,
          "lighthouseResult.audits.cumulative-layout-shift.numericValue"
        ),
        render_blocking_resources: _.get(
          psiResult,
          "lighthouseResult.audits.render-blocking-resources.numericValue"
        ),
        unused_js: _.get(
          psiResult,
          "lighthouseResult.audits.unused-javascript.numericValue"
        ),
        unused_js_potential_saving: _.get(
          psiResult,
          "lighthouseResult.audits.unused-javascript.details.overallSavingsBytes"
        ),
        unused_css: _.get(
          psiResult,
          "lighthouseResult.audits.unused-css-rules.numericValue"
        ),
        unused_css_potential_saving: _.get(
          psiResult,
          "lighthouseResult.audits.unused-css-rules.details.overallSavingsBytes"
        ),
        initial_server_response_time: _.get(
          psiResult,
          "lighthouseResult.audits.server-response-time.numericValue"
        ),
        defer_offscreen_images: _.get(
          psiResult,
          "lighthouseResult.audits.offscreen-images.details.overallSavingsBytes"
        ),
        impact_third_party_code: _.get(
          psiResult,
          "lighthouseResult.audits.third-party-summary.details.summary.wastedBytes"
        ),
        excessive_dom_size: _.get(
          psiResult,
          "lighthouseResult.audits.dom-size.numericValue"
        ),
        main_thread_work: _.get(
          psiResult,
          "lighthouseResult.audits.mainthread-work-breakdown.numericValue"
        ),
        not_efficient_assets_cache: _.get(
          psiResult,
          "lighthouseResult.audits.uses-long-cache-ttl.numericValue"
        ),
        javascript_execution_time: _.get(
          psiResult,
          "lighthouseResult.audits.bootup-time.numericValue"
        )
      };

      exportFileWriter.writeRecords([dataResult]);
      return resolve(true);
    } catch (error) {
      console.log(`CALL_EXCEPTION`, error);

      exportErrorFileWriter.writeRecords({
        host,
        path,
        fakeUrl,
        error: "CALL_EXCEPTION"
      });
    }

    return resolve(false);
  });
};

(async () => {
  const importFile = argv.file;
  const tries = argv.tries || 2;
  const initChunk = argv.chunk || 0;

  const importFilePath = path.resolve(__dirname, "..", importFile);
  const importFileContent = fs.readFileSync(importFilePath);
  const epxortFile = path.resolve(
    __dirname,
    "..",
    importFile.replace(".csv", "") + ".output.csv"
  );
  const epxortErrorFile = path.resolve(
    __dirname,
    "..",
    importFile.replace(".csv", "") + ".output.error.csv"
  );
  const statsFile = path.resolve(
    __dirname,
    "..",
    importFile.replace(".csv", "") + ".stats.csv"
  );

  const exportFileWriter = createExportFile(epxortFile);
  const exportErrorFileWriter = createExportFile(epxortErrorFile);

  if (fs.existsSync(statsFile)) {
    console.log("STATS FILE EXISTS");
  }

  const tmpResults = await parse(importFileContent, {
    columns: true,
    skip_empty_lines: true
  });
  const results = [];
  for await (const website of tmpResults) {
    results.push({
      type: website.type,
      host: website.host,
      path: website.path
    });
  }

  console.info(`Import File:           ${importFilePath}`);
  console.info(`Output File:           ${epxortFile}`);
  console.info(`Output Error File:     ${epxortErrorFile}`);
  console.info(`Total pages:           ${results.length}`);
  console.info(`Total tries generated: ${results.length * 2 * tries}`);
  console.info(``);

  const statisticValues = [];

  for (let x = 0; x < tries; x++) {
    for (let i = 0; i < results.length; i++) {
      const website = results[i];

      for (let k = 0; k < STATISTICS_STRATEGIES.length; k++) {
        const strategy = STATISTICS_STRATEGIES[k];

        statisticValues.push({
          ...website,
          strategy
        });
      }
    }
  }

  const chunks = _.chunk(statisticValues, GOOGLE_API_KEYS.length);

  for (let i = initChunk; i <= chunks.length; i++) {
    const start = new Date();
    console.log(`Chunk start --> ${i} / ${chunks.length}`);

    const chunk = chunks[i];

    if (!chunk) {
      continue;
    }

    const promises = [];
    chunk.forEach((res, index) => {
      const apiKey = GOOGLE_API_KEYS[index];

      promises.push(
        call(res, {
          apiKey,
          chunkId: i,
          exportFileWriter,
          exportErrorFileWriter
        })
      );
    });

    await Promise.all(promises);

    console.log(
      `Chunk End --> ${i} / ${chunks.length}  --> ends in ${differenceInSeconds(
        new Date(),
        start
      )}sec`
    );

    await sleep(2 * 1000)
  }

  console.log(`End`);
})();
