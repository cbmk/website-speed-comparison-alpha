require('console-stamp')(console, '[HH:MM:ss.l]');
const psi = require('psi');
const axios = require('axios');
const cheerio = require('cheerio');
const { text } = require('cheerio/lib/api/manipulation');


(async () => {

    const listingUrl = 'https://ca.autansa3000.com/coches/usados/' // PT
    
  const html = await axios.get(listingUrl).then(response => response.data)
  const $ = cheerio.load(html);

  let vdpUrl = ''
  
  const v3Regex = /<a href="([^"]*)" class="c-vehicle-card--row__media-grid /;
  let v3MatchResult = v3Regex.exec(html)

  if (v3MatchResult) {
    vdpUrl = v3MatchResult[1]
  }
  /**
   * Classic cards
   */
  else if ($('.c-vehicle-card__main').length > 0 ) {
    vdpUrl = $('.c-vehicle-card__main').first().attr('href')
  } else if ($('.c-vehicle-card--row__media-grid-holder').length > 0 ) {
    vdpUrl = $('.c-vehicle-card--row__media-grid-holder').first().attr('href')
  } else if ($('.car-card-stock__link-block').length > 0) {
    vdpUrl = $('.car-card-stock__link-block').first().attr('href')
  }
  /**
   * (R)Evolution cards
   */
  else if ($('.vcard--link').length > 0 ) {
    vdpUrl = $('.vcard--link').first().attr('href')
  }


  console.log(vdpUrl)
  return (vdpUrl.length > 0 ) ? vdpUrl : null





    /*
    const url = 'https://gs.statcounter.com/detect';

    const response = await psi(url, {
        key: 'AIzaSyBh8pj4c_h7GbvBZ01sLbOmP4TqngQkELg',
        strategy: 'mobile',
    });

    console.log(response);
/*
    const res = await axios.get(url, {
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4183.140 Safari/537.36',
        }
    })

    const $ = cheerio.load(res.data);


    const agent = $('em').text();

    console.log(agent);

 */
})();
